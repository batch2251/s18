x = sumAll(5, 15);

function sumAll() {
  let sum = 0;
  for (let i = 0; i < arguments.length; i++) {
    sum += arguments[i];
  }
  console.log('The Displayed sum of ' + 5 + ' and ' + 15 + ' is: ')
  console.log(sum);
}


function minus(x, y) {
  
  function print(ans) {
     console.log(ans)
  }
  const ans = x - y
  print(ans)
  return ans
}
console.log('Displayed difference of ' + 20 + ' and ' + 5 + ' is: ')
minus(20,5);



function multiplyNumbers(x,y){
        function print(ans) {
     console.log(ans)
  }
  const ans = x * y
  print(ans)
  return ans
}
console.log('The product of ' + 50 + ' and ' + 10 + ' is: ')
multiplyNumbers(50,10);



function divideNum(x,y){
        function print(ans) {
     console.log(ans)
  }
  const ans = x / y
  print(ans)
  return ans
}
console.log('The quotient of ' + 50 + ' and ' + 10 + ' is: ')
divideNum(50,10);





function circleArea(radius) {
    let area = Math.PI * (radius * radius);
    console.log(Math.round(area*100)/100);
}

console.log('The result of getting the area of a circle with 15 radius: ')
circleArea(15);


const grades = [20, 40, 60, 80];

function getAvg(grades) {
  const total = grades.reduce((acc, c) => acc + c, 0);
  return total / grades.length;
}

const average = getAvg(grades);
console.log('The average of 20, 40, 60 and 80: ');
console.log(average);



function test50(v, y)
{
  return ((v + y) > 35);
}

let v = 19
let y = 19
console.log('Is '+(v + y)+'/50 a passing score? ');
console.log(test50(v, y))