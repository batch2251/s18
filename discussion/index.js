// Function Parameters and Arguments

// "name" is called parameter that acts as a named variable/container that exist only inside of the function

// "Nehemiah" - argument - it is the information/data provided directly into the function.

function printInput(name) {
	console.log("My name is " + name);
};

printInput("Nehemiah");

printInput("Ruby");
printInput("CJ");


let sampleVariable = "Yui";

printInput(sampleVariable);


function checkDivisibilityBy8(num){
				let remainder = num % 8;
				console.log("The remainder of " + num + " divided by 8 is: " + remainder);
				let isDivisibleBy8 = remainder === 0;
				console.log("Is " + num + " divisible by 8?");
				console.log(isDivisibleBy8);
			}

			checkDivisibilityBy8(64);
			checkDivisibilityBy8(28);


// function params(name) {
// 	console.log("Hi, my name is " + name)
// }

// params(prompt("Enter your name"));



// Functions as Arguments

// Function parameters can also accept other functions as arguments.


function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunction){
	argumentFunction();
	console.log("this code comes from invokeFunction");
}

invokeFunction(argumentFunction);


// Multiple Arguments- will correspond to the number of "Parameters" declared in a function in succeeding ordeer.

function createFullName(firstName, middleName, lastName) {
	console.log(firstName + ' ' + middleName + ' ' + lastName);

}

createFullName('Juan', 'Dela');
createFullName('Jane', 'Dela', 'Cruz');
createFullName('Jane', 'Dela', 'Cruz', 'Hello');


// =======================================

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);


// The Return Statements

// The Return Statements - allows us to output a value from a function to be passed to the line/block of code that invoked/ called the function.

// The 'return' statement also stops the execution of the function and any code after the return statement will not be executed.

function returnFullname(firstName, middleName, lastName) {
	return firstName + ' ' + middleName + ' ' + lastName;

	console.log("This message will not be printed");
}
returnFullname('Joe', 'Jayson', 'Helberg');


function returnAddress(city, country) {
	let fullAddress = city + "," + country;
	return fullAddress;
}

let myAdress = returnAddress("Cebu City", "Philippines");


console.log(myAdress);